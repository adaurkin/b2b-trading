pragma solidity ^0.4.23;

contract Registry {
  address public owner;
  // participant info
  struct participant{
    string name;
    string inn;
    string publicKey;
    bool publisher;
    string bankAcc;
    string signatory;
    // ...
  }
  // public ?
  mapping(address => participant) public participants;
  address[] public participantAddresses;
  mapping (address => uint) public balances;

  constructor() public {
    owner = msg.sender;
  }

  modifier restricted() {
    if (msg.sender == owner) _;
  }

  // number of participants - for iteration
  function size() public view returns (uint) {
    return participantAddresses.length;
  }

  function getBalance(address addr) public view returns (uint) {
    return balances[addr];
  }
  // get participant info (can mutate with adding new struct members)
  function getParticipantInfo(address addr) public view returns (string, string,string) {
    return (participants[addr].name, participants[addr].inn, participants[addr].publicKey);
  }
  function getName(address addr) public view returns (string) {
    return participants[addr].name;
  }
  function getInn(address addr) public view returns (string) {
    return participants[addr].inn;
  }
  function getPublicKey(address addr) public view returns (string) {
    return participants[addr].publicKey;
  }
  function getBankAcc(address addr) public view returns (string) {
    return participants[addr].bankAcc;
  }
  function getSignatory(address addr) public view returns (string) {
    return participants[addr].signatory;
  }
  function isPublisher(address addr) public view returns (bool) {
    return participants[addr].publisher;
  }

  function checkInitialized(address addr, bool initialized) public view {
    // inn is the "flag"
    bytes memory tempEmptyStringTest = bytes(participants[addr].inn);
    if (initialized) {
      require(tempEmptyStringTest.length > 0);
    } else {
      require(tempEmptyStringTest.length == 0);
    }
  }
  // setting info
  // self service functions
  // create new participant (self service)
  function addParticipant(string _inn) public returns (bool) {
    // inn can not be changed! check if this address was used with any inn
    checkInitialized(msg.sender, false);
    // store the value
    participants[msg.sender].inn = _inn;
    participantAddresses.push(msg.sender);
    return true;
  }
  // set publicKey (self service) - can be changed at any time (old signatures should store old publicKey?)
  function setPublicKey(string _publicKey) public returns (bool) {
    // participant must exist (be initialized)
    checkInitialized(msg.sender, true);
    // store the value
    participants[msg.sender].publicKey = _publicKey;
    return true;
  }
  // set publicKey (self service) - can be changed at any time (old signatures should store old publicKey?)
  function setBankAcc(string _bankAcc) public returns (bool) {
    // participant must exist (be initialized)
    checkInitialized(msg.sender, true);
    // store the value
    participants[msg.sender].bankAcc = _bankAcc;
    return true;
  }
  // set publicKey (self service) - can be changed at any time (old signatures should store old publicKey?)
  function setSignatory(string _signatory) public returns (bool) {
    // participant must exist (be initialized)
    checkInitialized(msg.sender, true);
    // store the value
    participants[msg.sender].signatory = _signatory;
    return true;
  }
  // restricted functions
  // set name (owner only, or special oracles - maybe later) - can be changed at any time
  function setName(address addr, string _name) public restricted returns (bool) {
    // participant must exist (be initialized)
    checkInitialized(addr, true);
    // store the value
    participants[addr].name = _name;
    return true;
  }
  // set publisher (owner only) - can be changed at any time
  function setPublisher(address addr, bool _publisher) public restricted returns (bool) {
    // participant must exist (be initialized)
    checkInitialized(addr, true);
    // store the value
    participants[addr].publisher = _publisher;
    return true;
  }
}
