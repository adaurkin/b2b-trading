const Registry = artifacts.require("Registry.sol");

contract('Registry', async (accounts) => {
    describe('basic tests', function() {
        it("adding participants, counting", async () => {
            let instance = await Registry.new({from: accounts[0]});

            assert.equal(0, (await instance.size()).valueOf());
            await instance.addParticipant("inn1", {from: accounts[1]});
            assert.equal(1, (await instance.size()).valueOf());
            await instance.addParticipant("inn2", {from: accounts[2]});
            assert.equal(2, (await instance.size()).valueOf());
        });
        it("setting/getting info", async () => {
            let instance = await Registry.new({from: accounts[0]});

            assert.equal(0, (await instance.size()).valueOf());
            await instance.addParticipant("inn1", {from: accounts[1]});
            assert.equal("inn1", (await instance.getInn(accounts[1])).valueOf());

            await instance.setPublicKey("publicKey1", {from: accounts[1]});
            assert.equal("publicKey1", (await instance.getPublicKey(accounts[1])).valueOf());

            await instance.setBankAcc("bankAcc1", {from: accounts[1]});
            assert.equal("bankAcc1", (await instance.getBankAcc(accounts[1])).valueOf());

            await instance.setSignatory("signatory1", {from: accounts[1]});
            assert.equal("signatory1", (await instance.getSignatory(accounts[1])).valueOf());

            instance.getParticipantInfo(accounts[1]).then(function(response) {
              assert.equal("", response[0]);
              assert.equal("inn1", response[1]);
              assert.equal("publicKey1", response[2]);
            });
        });
        it("protected info", async () => {
            let instance = await Registry.new({from: accounts[0]});

            assert.equal(0, (await instance.size()).valueOf());
            await instance.addParticipant("inn1", {from: accounts[1]});

            // inn can't be changed
            try {
                await instance.addParticipant("inn2", {from: accounts[1]});
                assert.equal("Expected throw", "");
                console.log('not ok');
            } catch (error) {
                assert(error.message.search('revert') >= 0);
            }
        });
    });

    describe('access rights tests', function() {
        it("name", async function() {
            let instance = await Registry.new({from: accounts[0]});

            await instance.addParticipant("inn2", {from: accounts[2]});

            // empty fy default
            assert.equal("", (await instance.getName(accounts[2])));
            // participant can't change
            await instance.setName(accounts[2], "name2", {from: accounts[2]});
            assert.equal("", (await instance.getName(accounts[2])));
            // owner can change
            await instance.setName(accounts[2], "name2", {from: accounts[0]});
            assert.equal("name2", (await instance.getName(accounts[2])));
        });
        it("publisher", async function() {
            let instance = await Registry.new({from: accounts[0]});

            await instance.addParticipant("inn2", {from: accounts[2]});

            // false fy default
            assert.equal(false, (await instance.isPublisher(accounts[2])));
            // participant can't change
            await instance.setPublisher(accounts[2], true, {from: accounts[2]});
            assert.equal(false, (await instance.isPublisher(accounts[2])));
            // owner can change
            await instance.setPublisher(accounts[2], true, {from: accounts[0]});
            assert.equal(true, (await instance.isPublisher(accounts[2])));
            await instance.setPublisher(accounts[2], false, {from: accounts[0]});
            assert.equal(false, (await instance.isPublisher(accounts[2])));
        });
    });
});
