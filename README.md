# Электронная торговая площадка
## Смарт контракты + юридическая значимость

@TomskOpenHack 2018

[Презентация](https://gitpitch.com/adaurkin/b2b-trading/master?grs=gitlab&t=night)

## Технологии
* Ethereum
 * Solidity
 * web3.js
 * Truffle
* IPFS
* Javascript
* JSON
* Microsoft Azure
* ...
