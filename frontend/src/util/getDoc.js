// import Web3 from 'web3'
// import {address, ABI} from './constants/registryContract'
/*
let getDoc = new Promise(function (resolve, reject) {
  let web3 = new Web3(window.web3.currentProvider)
  let registryContract = web3.eth.contract(ABI)
  let registryContractInstance = registryContract.at(address)
  console.log(registryContract)
  console.log(registryContractInstance)
  resolve(registryContractInstance)
})
* /
function getDoc() {
  return '= getDoc =';
}
*/
const getDoc = 'Договор № {{docNum}}\nпоставки товара\n \n{{docPlace}}                                                                                   {{docDate}}\n \nНастоящий Договор заключен между Сторонами:\n{{company1}} в лице {{signatory1}} (далее – Заказчик)\nи {{company2}} в лице {{signatory2}} (далее – Поставщик).\n1. Предмет Договора\n1.1. Поставщик обязуется поставить {{docSubject}} (далее - «Товар») на условиях, в порядке и в сроки, определяемые Сторонами в настоящем Договоре, а Заказчик обязуется принять и оплатить Товар.\n1.2. Наименование, количество, характеристики Товара и сроки его поставки указаны в Спецификации (Приложение А), являющейся неотъемлемой частью настоящего Договора.\n1.3. На момент передачи Заказчику Товара последний должен принадлежать Поставщику на праве собственности и не должен находиться в залоге, под арестом, являться предметом исков третьих лиц.\n \n2. Цена по Договору и порядок расчётов\n2.1. Общая цена Договора составляет {{docAmount}} руб.\n\n...\n\n11. Подписание договора\n11.1. Договор подписывается в электронном виде с использованием квалифицированных электронных подписей Сторон.\n\n12. Банковские реквизиты и адреса Сторон\n12.1. В случае изменения адреса или обслуживающего банка Стороны обязаны в течение двух рабочих дней уведомить об этом друг друга.\n\nЗаказчик:\n{{company1}}\n{{address1}}\n{{bankAcc1}}\n{{signatory1}}\n\nПоставщик:\n{{company2}}\n{{address2}}\n{{bankAcc2}}\n{{signatory2}}';

// export default getDoc;
export default getDoc;
