// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// import App from './App';
import router from './router';
import { store } from './store/';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  // components: { App },
  // template: '<App/>',
  template: `
    <div id="app">
      <ul>
        <li><router-link to="/">Ваша компания</router-link></li>
        <li><router-link to="/template">Смарт шаблоны</router-link></li>
        <li><router-link to="/doc">Договоры</router-link></li>
        <li><router-link to="/participants">Участники</router-link></li>
        <li><router-link to="/deals">Торги</router-link>
          <ul>
            <li><router-link to="/new-deal">Создать</router-link></li>
          </ul>
          </li>
      </ul>
      <hr>
      <router-view class="view"></router-view>
    </div>
  `,
});
