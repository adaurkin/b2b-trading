import Vue from 'vue';
import Router from 'vue-router';
import dapp from '@/components/dapp';
import template from '@/components/template';
import doc from '@/components/doc';
import participants from '@/components/participants';
import deals from '@/components/deals';
deals

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'dapp',
      component: dapp,
    },
    {
      path: '/template',
      name: 'template',
      component: template,
    },
    {
      path: '/doc',
      name: 'doc',
      component: doc,
    },
    {
      path: '/participants',
      name: 'participants',
      component: participants,
    },
    {
      path: '/deals',
      name: 'deals',
      component: deals,
    },
  ],
});
